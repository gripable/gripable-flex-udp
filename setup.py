from setuptools import setup, find_packages
import codecs
import os.path


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), 'r') as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith('__version__'):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


name = find_packages()[1].replace(".","-")
package = name.split("-")[1]

setup(name=name,
      version=get_version(os.path.join("gripable", package, "__init__.py")),
      description='Gripable: ' + package,
      url='https://www.gripable.co',
      author='Gripable Ltd',
      author_email='matjaz@gripable.co',
      license='MIT',
      packages=find_packages(),
      namespace_packages=['gripable'],
      install_requires=[line.strip() for line in open("requirements.txt").readlines()],
      zip_safe=False)
