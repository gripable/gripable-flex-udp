# Gripable UDP clients
## What is this repository for?

Contains UDP clients for Gripable's motion tracking mobile apps. The package is pip-installable - package "udpclient" 
belongs to the namespace "gripable".

**Current Version**: see `gripable/udpclient/__init__.py`  
**v4.0.0:** Refactored to align with PEP8 styleguide
**v3.1.1:** CSV load scripts corrected
**v3.0.0:** Flex UDP: protobuf extracted into a separate repo  
**v2.2.0:** Flex UDP: csv file loader  
**v2.1.1:** Flex UDP: unix time replaced with uc clock  
**v2.0.1:** UdpThread filesaving bug fix  
**v2.0.0:** RGB client added and refact  
**v1.0.0:** UDP client

## How do I get set up?

Clone the repository
```bash
git clone https://bitbucket.org/gripable/gripable-udp.git
```
to install into your python environment, move inside the repository and run  
`pip install .`  
_Note:_  On Windows, pip may fail to install the netifaces package. It can be also installed through conda:
`conda install -c conda-forge netifaces`

## Test
Minimal working example to recieve sensor data from G-UDP app of one Flex device:  
```python
from gripable.flexudp.FlexUdp import FlexUdp
from gripable.flexudp.protobuf.SensorData_pb2 import SensorData
import time

flex = FlexUdp()
flex.start()
while True:
    try:
        time.sleep(.05)
        dev1data: SensorData = flex.getSensorData()
        print("\r grip: %f"%dev1data.grip.force, end="")
    except KeyboardInterrupt:
        print("ctrl+c")
        break
flex.stop()
flex.join()
```
or run `FlexUdp.py`  

See the source code for list of available  constructor parameters.

### Requirements and dependencies
* Python 3.6 or higher  
* See dependencies.txt

## Who do I talk to?

* [Matjaž Ogrinc](mailto:matjaz@gripable.co)

## Commit messages
Commit message must be prefixed with one of the following.
```
chore: add Oyster build script
docs: explain hat wobble
feat: add beta sequence
fix: remove broken confirmation message
refactor: share logic between 4d3d3d3 and flarhgunnstow
style: convert tabs to spaces
test: ensure Tayne retains clothing
```
