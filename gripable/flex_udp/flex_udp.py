import copy
import csv
import socket
import time

from overrides import overrides

from gripable.flex_udp.protobuf.SensorData_pb2 import SensorData
from gripable.flex_udp.udp_thread import UdpThread


def sensor_data_header_list() -> list:
    """ in addition to tRec """
    return "androidTime,timeStamp,acc_x,acc_y,acc_z,mag_x,mag_y,mag_z,quat_w,quat_x,quat_y,quat_z,force".split(",")


def sensor_data_to_list(sd: SensorData) -> list:
    return [sd.timestamp, sd.quaternion.sensorTimestamp,
            sd.accelerometer.x, sd.accelerometer.y, sd.accelerometer.z,
            sd.magnetometer.x, sd.magnetometer.y, sd.magnetometer.z,
            sd.quaternion.w, sd.quaternion.x, sd.quaternion.y, sd.quaternion.z,
            sd.grip.force]


class FlexUdp(UdpThread):

    def __init__(self, port: int = 4444, IP: str = None, file_path: str = None):
        """

        :param port: UDP port number, default is 4444
        :param IP: Client's internet address, if not provided it is automatically determined
        :param file_path: by default logging is disabled. Use %MAC to include MAC address in the filename
        """
        super().__init__(port, IP, file_path)
        self._mac = None

        """ file writing in parent class is more complicated, therefore this class
            only uses the self._filePath and self._newDataToFile() from the parent """
        self.__fileObj = None
        self.__wr = None

    def get_sensor_data(self) -> SensorData:
        with self._lock:
            if self._devUdpData is None:
                return None
            else:
                return copy.deepcopy(self._devUdpData[0])

    @overrides
    def _receive_data(self) -> list:
        try:
            data, _ = self._udpSocket.recvfrom(128)
            if len(data) == 87 + 17:
                # self._androidTime = struct.unpack('>q', data[:8])
                sdLen = data[17]
                sd = SensorData.FromString(data[18:(18 + sdLen)])
                if self._mac is None:
                    self._mac = str(data[:17], 'utf-8')

                return [sd]
            else:
                print(f"wrong packet size: {len(data)}")
                return None
        except socket.timeout:
            print("timed out")
            return []

    @overrides
    def new_data_to_file(self):
        if self.__fileObj is None:
            filePath = self._filePath.replace("%MAC", self._mac.replace(':', '-'))
            self.__fileObj = open(filePath, mode='w')
            self.__wr = csv.writer(self.__fileObj, delimiter='\t', lineterminator='\n')
            self.__wr.writerow(["tRec"] + sensor_data_header_list())

        sd = self._devUdpData[0]
        self.__wr.writerow([self._timeReceived] + sensor_data_to_list(sd))

    @overrides
    def _close_comms(self):
        super()._close_comms()
        if self.__fileObj is not None:
            self.__fileObj.close()

    def get_mac(self):
        return self._mac


if __name__ == "__main__":
    flex = FlexUdp()
    flex.start()
    while True:
        try:
            time.sleep(.05)
            dev1data: SensorData = flex.get_sensor_data()
            if dev1data is not None:
                print("\r grip: %f" % dev1data.grip.force, end="")

        except KeyboardInterrupt:
            print("ctrl+c")
            break
    flex.stop()
    flex.join()
