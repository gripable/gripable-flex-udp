import warnings

import numpy as np
import pandas as pd

from gripable.flex_udp.protobuf.binary_unpack import unwrap_counter


def unwrap_counter(counter, bits=16):
    overflow = np.zeros(counter.shape)
    overflow[1:] = np.diff(counter) < 0
    return counter + np.cumsum(overflow) * (2 ** bits)


def load_flex_csv(file_path: str, correct_time_scale=True, interp_acc=True):
    """

    @param file_path:
    @param correct_time_scale:
    @param interp_acc:
    @return: epoch relative to client clock, epoch relative to server, ...
    """
    df = pd.read_csv(file_path, delimiter="\t")

    # udp client clock
    t_client = df.tRec.values

    # source (microcontroller) time
    t_uc = unwrap_counter(df.timeStamp.values) / 1000.
    t_uc -= t_uc[0]

    if "androidTime" in df.keys():
        t_server = df.androidTime / 1000
    else:
        warnings.warn("no android time record")
        t_server = t_client

    if correct_time_scale:
        # It should be corrected based on server unix time (androidTime)
        tScale = np.ptp(t_server[10:-10]) / np.ptp(t_uc[10:-10])
        if .95 < tScale < 1.05:
            t_uc *= tScale
        else:
            warnings.warn("Could not estimate timescale")
    t_inferred = t_client[0] + t_uc

    force = df.force.values
    acc = df[["acc_" + sub for sub in 'xyz']].values
    mag = df[["mag_" + sub for sub in 'xyz']].values
    try:
        import quaternion
        qua = quaternion.from_float_array(df[["quat_" + sub for sub in 'wxyz']])
    except ImportError:
        warnings.warn(
            "Returning quaternions as numpy array. Recommended install: https://quaternion.readthedocs.io/en/latest/")
        qua = df[["quat_" + sub for sub in 'wxyz']].values

    # interpolate acc from 25Hz to 50Hz. Mag is sampled at ~10 Hz anyway
    if interp_acc:
        last = 0 if len(acc) % 2 == 1 else -1
        acc[1:(last - 1):2] = acc[0:(last - 2):2] * .5 + acc[2::2] * .5
    # mag[1:(last - 1):2] = mag[0:(last - 2):2] * .5 + mag[2::2] * .5

    return t_inferred, t_server, acc, mag, qua, force


def load_flex_csv_force(file_path: str, correct_time_scale=True):
    df = pd.read_csv(file_path, delimiter="\t")

    # client unix time
    tRec = df.tRec.values

    # source (microcontroller) time
    tSent = unwrap_counter(df.timeStamp.values) / 1000.

    if "androidTime" in df.keys():
        tServer = df.androidTime / 1000
    else:
        tServer = tSent

    if correct_time_scale:
        # It should be corrected based on server unix time (sent)
        tScale = np.ptp(tServer) / np.ptp(tSent)
        tSent *= tScale
    t = tRec[0] + tSent - tSent[0]

    force = df.force.values

    return t, force