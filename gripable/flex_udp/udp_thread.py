import copy
import socket
import threading
import time
from overrides import overrides

class UdpThread(threading.Thread):
    def __init__(self,
                 port: int = 8087,
                 IP: str = None,
                 filePath: str = None):

        try:
            import netifaces
        except ImportError:
            print("No IP provided and netifaces not installed")

        # data
        self._timeReceived = 0.
        self._devUdpData = None  # list of received data

        # thread management
        self._packetRate = -1
        self.__shouldRun = True
        self.__printDataPeriod = 1.
        self._shouldPrint = True
        self._lock = threading.Lock()

        # file writing
        self._filePath = filePath

        """ file writing example
        self.__fileObjs = []
        self.__fileWriters = []
        """
        # hardware
        self._configure_communication(IP, port)

        threading.Thread.__init__(self)

    def _get_tag(self):
        return self.__class__.__name__ + ": "

    def set_should_print(self, p):
        self._shouldPrint = p

    def _configure_communication(self, IP, port):
        if not IP:
            print(self._get_tag(), 'No network interface IP provided, guessing')
            try:
                import netifaces
                # if tethering the IP should be 192.168.42.y
                for iface in netifaces.interfaces():
                    if netifaces.AF_INET in netifaces.ifaddresses(iface):
                        ip = netifaces.ifaddresses(iface)[netifaces.AF_INET][0]['addr']
                        if ip != "127.0.0.1":
                            IP = ip
                            if '192.168.42' in IP:
                                print(self._get_tag(), 'tethered on usb!')
                                break

            except ImportError:
                print("package netifaces not found, using socket to determine IP ")
                IP = socket.gethostbyname(socket.gethostname())

            if not IP:
                raise ConnectionError("Check networking")
            else:
                print(self._get_tag(), f"trying {IP}")

        self._udpSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._udpSocket.bind((IP, port))
        self._udpSocket.settimeout(5.0)
        self.__devTimeMsPrev = 0
        self.__counterUnwrap = 0

    def _receive_data(self) -> list:
        raise NotImplementedError
        """ example
        try:
            data, _ = self._udpSocket.recvfrom(2048)
            serializedGripables = json.loads(data)['SerializedGripables']
            # for debugging
            self.lastjson = data
            newDevices = []
            for gripable in serializedGripables:
                pass
                #device = GripableUdpDevData(**gripable)
                #newDevices.append(device)

            return newDevices

        except KeyError:
            print("udp json unpack error")
            return []
        except socket.timeout:
            print("timed out")
            return []
        """

    def _close_comms(self):
        self._udpSocket.close()
        del self._udpSocket
        print(self._get_tag(), "socket closed")

    @overrides
    def run(self):
        print(self._get_tag(), "Starting loop")
        lastPrint = 0
        # timePrev = None
        packetCount = 0
        while self.__shouldRun:
            newData = self._receive_data()
            if newData is not None and len(newData) > 0:
                self._timeReceived = time.time()
                self.process_new_data(newData)
                self._devUdpData = newData
                packetCount += 1

                """ save to file """
                if self._filePath is not None:
                    self.new_data_to_file()

                time.sleep(self.get_thread_sampling_time())

            if 0. < self.__printDataPeriod < (time.time() - lastPrint):
                self._packetRate = packetCount / self.__printDataPeriod
                packetCount = 0
                lastPrint = time.time()
                if self._shouldPrint:
                    print(self._get_tag(), 'rate %d Hz' % self._packetRate)
                    # print('IMU: ' + self.__task.getPrintString())

        print(self._get_tag(), "Loop finished")
        self.__clean_up()

    def get_thread_sampling_time(self):
        return 1. / 200.

    def process_new_data(self, newData):
        # called before newData is written to self._devUdpData to allow processing of both
        pass

    def new_data_to_file(self):
        """
        if len(self.__fileObjs) == 0:
            self.__generateLogFiles()
        for dev, writer in zip(self._devUdpData, self.__fileWriters):
            writer.writerow([self._timeReceived % (24 * 60 * 60)] +
                            dev.getListOfFloats(self.__recordRaw,
                                                self.__recordQuat,
                                                self.__recordForce))
        """
        raise NotImplementedError("TBD")

    def __clean_up(self):
        if self._filePath is not None:
            pass
            """for o in self.__fileObjs:
                o.close()
            """
        self._close_comms()

    def get_packet_rate(self):
        with self._lock:
            return self._packetRate

    def get_udp_data(self) -> list:
        with self._lock:
            if self._devUdpData is None:
                return []
            return [copy.deepcopy(dev) for dev in self._devUdpData]

    def stop(self):
        with self._lock:
            self.__shouldRun = False


