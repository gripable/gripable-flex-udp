# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 15:28:17 2020

@author: Matjaz
"""
import os

import numpy as np
from google.protobuf.message import DecodeError
from gripable.flex_udp.protobuf.SensorData_pb2 import SensorData

PROTO_MAX_LEN = 86
MAC_LEN = 17


def unwrap_counter(counter, bits=16):
    overflow = np.zeros(counter.shape)
    overflow[1:] = np.diff(counter) < 0
    return counter + np.cumsum(overflow) * (2 ** bits)


def sensor_data_to_tuple(sd: SensorData) -> tuple:
    return (sd.timestamp, sd.quaternion.sensorTimestamp, sd.grip.force,
            [sd.accelerometer.x, sd.accelerometer.y, sd.accelerometer.z],
            [sd.magnetometer.x, sd.magnetometer.y, sd.magnetometer.z],
            [sd.quaternion.w, sd.quaternion.x, sd.quaternion.y, sd.quaternion.z]
            )

def __sensor_datas_to_arrays(sds: list) -> tuple:
    t_unix = np.empty_like(sds, dtype=float)
    t_sens = np.empty_like(sds, dtype=float)
    force = np.empty_like(sds, dtype=float)
    acc = np.tile(t_unix, (3, 1)).T
    mag = np.tile(t_unix, (3, 1)).T
    quat_f = np.tile(t_unix, (4, 1)).T
    for i, sd in enumerate(sds):
        (t_unix[i], t_sens[i], force[i],
         acc[i], mag[i], quat_f[i]) = sensor_data_to_tuple(sd)
    try:
        import quaternion
        quat_f = quaternion.from_float_array(quat_f)
    except ImportError:
        pass
    t_sens = unwrap_counter(t_sens) / 1000
    t_unix /= 1000

    return t_unix, t_sens, force, acc, mag, quat_f


def __load_padded_pkg(filePath: str, pkgBytesMax: int = PROTO_MAX_LEN) -> tuple:
    with open(filePath, "rb") as f:
        data = f.read()

    mac = data[:17]

    packets = []

    for l in np.arange(17, len(data), pkgBytesMax + 1):  # +1 for byte count
        pLen = data[l]
        packet = data[l + 1:l + 1 + pLen]
        # packets.append(packet)
        try:
            sd = SensorData.FromString(packet)
            packets.append(sd)

        except DecodeError:
            print(f"decode error at package no.{l} of length {pLen} B")

    if len(packets) == 0:
        raise AttributeError("File has no data")

    return (mac.decode("utf-8"),) + __sensor_datas_to_arrays(packets)


def load_binary_file(filePath: str, correct_clock: bool = True, deinterlace: bool = True) -> tuple:
    """ Example:
    mac, t_unix, t_sens, force, acc, mag, quat = loadBinary("jajo.bin")

    :param deinterlace: Interpolate mag and acc
    :param correct_clock: if true, microcontroller time is scaled to match range of android unix time.
    :param filePath:
    :return: tuple of sensor data
    """
    if os.path.getsize(filePath) % (PROTO_MAX_LEN + 1) == 17:
        mac, t_unix, t_sens, force, acc, mag, quat = __load_padded_pkg(filePath)
        t_sens -= t_sens[0]
        if correct_clock:
            # correct timescale
            t_scale = np.ptp(t_unix) / t_sens[-1]
            print("scale: ", t_scale)
            t_sens *= t_scale
        if deinterlace:
            # interpolate acc from 25Hz to 50Hz. Mag is oversampled as it is
            last = 0 if len(acc) % 2 == 1 else -1
            acc[1:(last - 1):2] = acc[0:(last - 2):2] * .5 + acc[2::2] * .5
            # mag[1:(last - 1):2] = mag[0:(last - 2):2] * .5 + mag[2::2] * .5
        return mac, t_unix, t_sens, force, acc, mag, quat
    else:
        raise ValueError("bad file")


